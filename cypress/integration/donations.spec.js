context('Donations', () => {

  it('finds the content "DONACIONES"', () => {
    cy
      .visit('127.0.0.1:5500')
      .get('#trello-title-donaciones')
      .contains('DONACIONES')
  })

})
